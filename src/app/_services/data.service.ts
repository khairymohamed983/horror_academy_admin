import { Injectable } from '@angular/core';
import { HttpService } from './http.service';

@Injectable()
export class DataService {
    api: string = '';

    constructor(private http: HttpService) { }

    setCurrent(api) {
        this.api = api;
    }



    getBy(url) {
        console.log(url);
        return this.http.get(url)
            .map((res) => {
                return res.json();
            });
    }



    get(url) {
        return this.http.get(url)
            .map((res) => {
                return res.json();
            });
    }



    post(url, data) {
        return this.http.post(url, data)
            .map((res) => {
                return res.json();
            });
    }

    Update(url, data) {
        return this.http.post(url, data)
            .map((res) => {
                return res.json();
            });


    }

    getAll() {
        return this.http.get(`/api/${this.api}/all`)
            .map((res) => {
                return res.json();
            });
    }

    add(obj) {
        return this.http.post(`/api/${this.api}/add`, obj)
            .map((res) => {
                return res.json();
            });
    }


    update(obj) {
        return this.http.post(`/api/${this.api}/update`, obj)
            .map((res) => {
                return res.json();
            });
    }


    deleteAll() {
        return this.http.delete(`/api/${this.api}/deleteAll`)
            .map((res) => { });
    }

    delete(obj) {
        return this.http.post(`/api/${this.api}/delete`, obj)
            .map((res) => { });
    }

    Delete(url, obj) {
        return this.http.post(url, obj)
            .map((res) => { });
    }

}
