import { Injectable } from '@angular/core';
import { HttpService } from './http.service';

@Injectable()
export class UserService {
  constructor(private http: HttpService) { }

  login(username: string, password: string) {
    return this.http.post('/api/users/login', { email: username, password: password })
      .map((res) => {
        const user = res.json();
        console.log(user);
        if (user && user.token) {
          // localStorage.setItem('currentUser', JSON.stringify(user));
          localStorage.setItem('auth_token', user.token);
        }
      });
  }

  getAll () {
    return this.http.get('/api/users/all')
    .map((res) => {
      return res.json();
    });
  }

  logout() {
    localStorage.removeItem('auth_token');
  }

  isLoggedIn() {
    return localStorage.getItem('auth_token');
  }
  // token will added automatically to get request header
  getUser(id: number) {
    return this.http.get(`/api/users/${id}`).map((res) => {
      return res.json();
    });
  }
  getMe() {
    return this.http.get(`/api/me`).map((res) => {
      return res.json();
    });
  }
}
