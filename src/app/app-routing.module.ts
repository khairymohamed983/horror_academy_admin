import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './/layouts/layout.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { LockscreenComponent } from './pages/lockscreen/lockscreen.component';
import { Error404Component } from './pages/error-404/error-404.component';
import { Error500Component } from './pages/error-500/error-500.component';
import { ChampionshipComponent } from './pages/championship/championship.component';
import { ApplicationsComponent } from './pages/applications/applications.component';
import { NewsComponent } from './pages/news/news.component';
import { GallaryComponent } from './pages/gallary/gallary.component';
import { PartnersComponent } from './pages/partners/partners.component';
import { LiveComponent } from './pages/live/live.component';
import { ContactsComponent } from './pages/contacts/contacts.component';
import { MessagesComponent } from './pages/messages/messages.component';
import { AboutComponent } from './pages/about/about.component';
import { CommissionsComponent } from './pages/commissions/commissions.component';
import { SingleCommissionComponent } from './pages/single-commission/single-commission.component';

import { AuthGuard } from './_guards/auth.guard'
import { PlayersComponent } from './pages/players/players.component';
import { RecordsComponent } from './pages/records/records.component';

const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    {
        "path": "",
        "component": LayoutComponent,
        canActivate: [AuthGuard],
        "children": [
            {
                "path": "home",
                "component": HomeComponent
            },
            {
                "path": "applications",
                "component": ApplicationsComponent
            },
            {
                "path": "news",
                "component": NewsComponent
            },
            {
                "path": "players",
                "component": PlayersComponent
            },
            {
                "path": "records",
                "component": RecordsComponent
            },          
            {
                "path": "partners",
                "component": PartnersComponent
            },
            {
                "path": "live",
                "component": LiveComponent
            },
            {
                "path": "contactinfo",
                "component": ContactsComponent
            },
            {
                "path": "messages",
                "component": MessagesComponent
            },
            {
                "path": "gallary",
                "component": GallaryComponent
            },
            {
                "path": "about",
                "component": AboutComponent
            },
            {
                "path": "commissions",
                "component": CommissionsComponent
            },
            {
                "path": "single-commission",
                "component": SingleCommissionComponent
            },
            {
                "path": "championship",
                "component": ChampionshipComponent
            },
        ]
    },
    {
        "path": "login",
        "component": LoginComponent
    },
    {
        "path": "lockscreen",
        canActivate: [AuthGuard],
        "component": LockscreenComponent
    },
    {
        "path": "error_404",
        "component": Error404Component
    },
    {
        "path": "error_500",
        "component": Error500Component
    },
    {
        "path": "**",
        "redirectTo": "error_404",
        "pathMatch": "full"
    },
];

@NgModule({
    declarations: [

    ],
    imports: [RouterModule.forRoot(routes)],
    exports: [
        RouterModule,
    ]
})

export class AppRoutingModule { }
