import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

 
import { AppComponent } from './/app.component';
import { AppRoutingModule } from './/app-routing.module';
import { LayoutModule } from './/layouts/layout.module';
import { ScriptLoaderService } from './_services/script-loader.service';
import { ApplicationsComponent } from './pages/applications/applications.component';
import { NewsComponent } from './pages/news/news.component';
import { GallaryComponent } from './pages/gallary/gallary.component';
import { ChampionshipComponent } from './pages/championship/championship.component';
import { PartnersComponent } from './pages/partners/partners.component';
import { LiveComponent } from './pages/live/live.component';
import { ContactsComponent } from './pages/contacts/contacts.component';
import { MessagesComponent } from './pages/messages/messages.component';
import { AboutComponent } from './pages/about/about.component';
import { SingleCommissionComponent } from './pages/single-commission/single-commission.component';
import { CommissionsComponent } from './pages/commissions/commissions.component';
import { RepoImageUploaderComponent } from './custom/repo-image-uploader/repo-image-uploader.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserService } from './_services/user.service';
import { HttpService } from './_services/http.service';
import { XHRBackend, RequestOptions, HttpModule } from '@angular/http';
import { AuthGuard } from './_guards/auth.guard';
import { LoginComponent } from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { LockscreenComponent } from './pages/lockscreen/lockscreen.component';
import { Error404Component } from './pages/error-404/error-404.component';
import { Error500Component } from './pages/error-500/error-500.component';
import { DataService } from './_services/data.service';
import { PlayersComponent } from './pages/players/players.component';
import { RecordsComponent } from './pages/records/records.component';
import { ProfileComponent } from "./pages/profile/profile.component";
import { ForgotPasswordComponent } from "./pages/forgot-password/forgot-password.component";

import { MyDatePickerModule } from 'mydatepicker';
import { NgxEditorModule } from 'ngx-editor';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { HttpClientModule } from '@angular/common/http'; 
import { ToastrModule } from 'ngx-toastr';
import { NgDatepickerModule } from 'ng2-datepicker';


 




@NgModule({
    declarations: [
        AppComponent,
        ApplicationsComponent,
        PlayersComponent,
        HomeComponent,
        ProfileComponent,
        ForgotPasswordComponent,
        LoginComponent,
        LockscreenComponent,
        Error404Component,
        Error500Component,
        NewsComponent,
        GallaryComponent,
        ChampionshipComponent,
        PartnersComponent,
        LiveComponent,
        ContactsComponent,
        MessagesComponent,
        AboutComponent,
        SingleCommissionComponent,
        CommissionsComponent,
        RepoImageUploaderComponent,
        RecordsComponent
       
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        FroalaEditorModule.forRoot(), FroalaViewModule.forRoot(),
        HttpModule,
        LayoutModule,
        MyDatePickerModule,
        FormsModule,
        NgxEditorModule ,
        TooltipModule.forRoot(),
        HttpClientModule,        
        ToastrModule.forRoot(),
        NgDatepickerModule
    ],
    providers: [
        {
            provide: HttpService,
            useFactory: (backend: XHRBackend, options: RequestOptions) => {
                return new HttpService(backend, options);
            },
            deps: [XHRBackend, RequestOptions]
        }, AuthGuard,
        ScriptLoaderService, UserService, DataService],
    bootstrap: [AppComponent],
    exports: [RepoImageUploaderComponent]
})
export class AppModule { }
