import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

// import * as fs from 'fs';
// import * as path from 'path';
        

@Component({
  selector: 'app-repo-image-uploader',
  templateUrl: './repo-image-uploader.component.html',
  styleUrls: ['./repo-image-uploader.component.css']
})
export class RepoImageUploaderComponent implements OnInit {

  @Input() url: string = null;
  @Input() desc: string;
  @Output() imgChanged = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {

  }

  readUrl(event: any) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: any) => {
        console.log("event:" + event);
        this.url = event.target.result;

        
        // let base64Image = event.target.result.split(';base64,').pop();
        // fs.writeFile('../../assets/images/image.png', base64Image, {encoding: 'base64'}, function(err) {
        //     console.log('File created');
        // });

        this.imgChanged.emit(this.url);
      }
      reader.readAsDataURL(event.target.files[0]);
    }
  }

}
