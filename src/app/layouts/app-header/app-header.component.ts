import { Component, AfterViewInit } from '@angular/core';
import { UserService } from "../../_services/user.service";
import { Router } from '@angular/router';

@Component({
  selector: '[app-header]',
  templateUrl: './app-header.component.html',
})
export class AppHeader implements AfterViewInit {

  constructor(
    private usrSrvc: UserService,
    private router: Router
  ) { }

  ngAfterViewInit() {
  }

  logout() {
    this.usrSrvc.logout();
    this.router.navigate(['/login']);
  }
}
