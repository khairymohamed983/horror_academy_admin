import { Component, Output} from '@angular/core';
import { DataService } from "../../_services/data.service";
import { Router } from '@angular/router';
@Component({
    selector: '[app-sidebar]',
    templateUrl: './app-sidebar.component.html'
})
export class AppSidebar {

    commisions: any[] = [];
    constructor(private data: DataService, private router: Router) {
        this.data.setCurrent('commisions');
    }

    ngOnInit() {
        this.data.getAll().subscribe((commissions) => {
            this.commisions = commissions;
        })
    }

    //gotoCommission(com) {
    //  localStorage.setItem('currentCommission', JSON.stringify(com));
    //  this.router.navigate(['single-commission']);
    //}


 
    ShowAndEdit(com) {



        console.log("send data between component");
        localStorage.setItem('currentCommission', JSON.stringify(com));
        console.log(JSON.parse(localStorage.getItem('currentCommission')));

 
        window.location.reload();

    }

    
}
