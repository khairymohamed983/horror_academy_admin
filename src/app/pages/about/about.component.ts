﻿import { Component, OnInit } from '@angular/core';
import { DataService } from "../../_services/data.service";
import { ToastrService } from 'ngx-toastr';



@Component({
    selector: 'app-about',
    templateUrl: './about.component.html',
    styleUrls: ['./about.component.css']
})

export class AboutComponent implements OnInit {

    vision: string = '';
    target: string = '';

    constructor(private data: DataService, private toastr: ToastrService) { }

    ngOnInit() {
        this.data.get('/api/other/about').subscribe((about) => {
             this.vision = about[0].vission;
             this.target = about[0].goal;
        });
    }

    ngAfterViewInit() {
    }

    save() {

        this.data.post('/api/other/saveAbout', { vission: this.vision, goal: this.target }).subscribe((about) => {
            this.showSuccess();
            console.log(about);
        });
    }

    showSuccess() {
        this.toastr.success('نجاح', 'تم حفظ البيانات');
    }

    showError() {
        this.toastr.error('اخفاق ', 'فشل حفظ البيانات');
    }
}
