﻿import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { DataService } from "../../_services/data.service";
import { NgForm } from '@angular/forms';
import { Element } from '@angular/compiler';
import { ToastrService } from 'ngx-toastr';



@Component({
    selector: 'app-applications',
    templateUrl: './applications.component.html',
    styleUrls: ['./applications.component.css']
})
export class ApplicationsComponent implements OnInit {

    subscriptions = [];
    @ViewChild('tab1') tab1: ElementRef;

    stateMachine = false;

    loading = false;

    item = {
        name: null,
        email: null,
        mobile: null,
        address: null,
        district: null,
        birthdate: null,
        description: null,
        sourceKnowing: null,
        status: null
    }

    constructor(private data: DataService, private toastr: ToastrService) {
        this.data.setCurrent('subscriptions');
    }


    onUpdate(obj) {
        debugger
        this.data.update(obj)
            .subscribe(
            data => {
                this.getBy(obj.status);
                this.loading = false;
                this.showSuccess();
            },
            error => {
                this.loading = false;
                this.showError();

            });
    }


    getBy(status) {
        debugger
        this.data.getBy(`/api/subscriptions/byStatus?status=${status}`).subscribe((data) => {
            this.subscriptions = data;
        });

        if (status == "pending") {
            this.stateMachine = false;
        }
        else {
            this.stateMachine = true;
        }

    }

    accept(obj, index) {
        debugger
        this.subscriptions[index].status = 'accepted';
        this.onUpdate(obj);
        this.stateMachine = true;
    }

    reject(obj, index) {
        debugger
        this.subscriptions[index].status = 'rejected';
        this.onUpdate(obj);
        this.stateMachine = true;
    }

    ngOnInit() {
        this.getBy('pending');
    }

    showSuccess() {
        this.toastr.success('نجاح', 'تم حفظ البيانات');
    }

    showError() {
        this.toastr.error('اخفاق ', 'فشل حفظ البيانات');
    }

}
