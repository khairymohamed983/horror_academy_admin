﻿import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { DataService } from "../../_services/data.service";
import { NgForm } from '@angular/forms';
import { debug } from 'util';
import { ToastrService } from 'ngx-toastr';


@Component({
    selector: 'app-commissions',
    templateUrl: './commissions.component.html',
    styleUrls: ['./commissions.component.css']
})
export class CommissionsComponent implements OnInit {

    commissions = [];
    members = [];
    loading = false;
    isForm = false;
    isEditMember = false;
    @ViewChild('PopUp') PopUp: ElementRef;



    item = {
        name: "",
        image: "",
        decription: "",
        members: []

    }



    item2 = {
        memberName: "",
        position: "",
        title: "",
        image: "",
        description: ""
    }




    isEdit = false;

    constructor(private data: DataService, private toastr: ToastrService) {
        this.data.setCurrent('commisions');
    }

    onImageChanged(image: string) {
        this.item.image = image;
    }


    onImageChangedMembers(image: string) {
        this.item2.image = image;
    }



    onSubmit(f: NgForm) {
        
        if (f.valid) {
            this.loading = true;
            if (this.isEdit) {  // edit
                this.data.update(this.item)
                    .subscribe(
                        data => {
                            this.ngOnInit();
                            this.showSuccess();
                            f.reset();
                            this.reset();
                            this.resetMember();
                            this.isEdit = false;
                            this.isForm = false;
                            this.loading = false;
                            this.members=[];
                        },
                        error => {
                            this.loading = false;
                            this.showError();
                        });
            } else {  // add new 

                this.item.members.push(this.members);
                this.data.add(this.item)
                    .subscribe(
                        data => {
                            this.ngOnInit();
                            this.showSuccess();
                            f.reset();
                            this.reset();
                            this.members = [];
                            this.loading = false;
                            this.isForm = false;
                        },
                        error => {
                            this.loading = false;
                            console.log(error);
                            this.showError();
                        });
            }
        }
    }




    markAsDelete(it) {
        this.item = it;
    }



    reset() {
        this.item = {
            name: "",
            image: "",
            decription: "",
            members: []
        }
    }


    toggleView(isForm) {

        this.isEdit = false;
        this.isForm = isForm;
        this.reset();
        this.members = [];

    }



    removeItem() {
        this.data.delete(this.item)
            .subscribe(
                () => {
                    this.showDeletd();
                    this.isForm = false;
                    this.reset();
                    this.ngOnInit();
                },
                error => {
                    this.showError();
                    this.loading = false;
                });
    }

    editItem(obj) {
        
        this.isEdit = true;
        this.isForm = true;
        this.item = obj;
        this.members = obj.members;

    }

    ngOnInit() {
        this.data.getAll().subscribe((data) => {
            this.commissions = data;
        })
    }



    onSubmitMember(f: NgForm) {
        
        if (!this.isEditMember) {  // add
            this.members.push(this.item2);
        }
        this.resetMember();
        this.popUpDismiss();
    }


    resetMember() {
        this.item2 = {
            memberName: "",
            position: "",
            title: "",
            image: "",
            description: ""
        }

    }

    deleteMember(index) {
        if (index >= 0) {
            this.members.splice(index, 1);
        }
    }


    updateMember(obj) {
        
        this.item2 = obj;
        this.isEditMember = true;
    }


    popUpDismiss() {
        this.PopUp.nativeElement.click();
    }

    showSuccess() {
        this.toastr.success('نجاح', 'تم حفظ البيانات');
    }

    showError() {
        this.toastr.error('اخفاق ', 'فشل حفظ البيانات');
    }
    showDeletd() {
        this.toastr.success('تم الحذف', 'نجاح');
    }

}
