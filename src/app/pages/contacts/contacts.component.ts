﻿import { Component, OnInit } from '@angular/core';
import { DataService } from "../../_services/data.service";
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-contacts',
    templateUrl: './contacts.component.html',
    styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {

    loading = true;
    item = {
        address: null,
        phone: null,
        whatsApp: null,
        twitter: null,
        instagram: null,
        snapshat: null,
        image: null
    }



    constructor(private data: DataService, private toastr: ToastrService) {
        this.data.setCurrent('contacts');
    }

    onImageChanged(image: string) {
        this.item.image = image;
    }

    onSubmit(f: NgForm) {
        if (f.valid) {
            this.loading = true;
            this.data.add(this.item)
                .subscribe(
                    data => {
                        this.showSuccess();
                        this.ngOnInit();
                        this.reset();
                        this.loading = false;
                    },
                    error => {
                        this.loading = false;
                        this.showError();
                    });
        }
    }

    markAsDelete(it) {
        this.item = it;
    }

    reset() {
        this.item = {
            address: null,
            phone: null,
            whatsApp: null,
            twitter: null,
            instagram: null,
            snapshat: null,
            image: null
        }
    }

    ngOnInit() {
        this.data.getAll().subscribe((data) => {
            console.log(data);
            this.item = data[0];
        })
    }

    showSuccess() {
        this.toastr.success('نجاح', 'تم حفظ البيانات');
    }

    showError() {
        this.toastr.error('اخفاق ', 'فشل حفظ البيانات');
    }

}

