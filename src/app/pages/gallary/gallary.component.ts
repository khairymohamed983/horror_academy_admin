﻿import { Component, OnInit } from '@angular/core';
import { DataService } from "../../_services/data.service";
import { NgForm } from '@angular/forms';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { debug } from 'util';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-gallary',
    templateUrl: './gallary.component.html',
    styleUrls: ['./gallary.component.css']
})
export class GallaryComponent implements OnInit {

    images = [];
    videos = [];


    loading = false;
    isForm = false;
    isImage = false;
    isVideo = false;
    isEdit = false;
    showOne = true;
    loadingvideo = false;
    currentUrl: SafeResourceUrl;
    currentUrlPop: SafeResourceUrl;






    itemVideo = {
        title: "",
        videoUrl: "",
        decription: "",
        isYoutube: true
    }

    itemImage = {
        title: "",
        image: "",
        decription: ""
    }



    constructor(private data: DataService, public domsant: DomSanitizer, private toastr: ToastrService) {
        // this.data.setCurrent('champions');
    }

    urlChanged($event) {
        let parts = this.itemVideo.videoUrl.split('=');
        let id = '';
        if (parts && parts.length) {
            id = parts[1].split('&')[0];
        }
        let fullUrl = `https://www.youtube.com/embed/${id}`;
        this.loadingvideo = true;
        this.currentUrl = this.domsant.bypassSecurityTrustResourceUrl(fullUrl);
    }

    adjustUrl(url) {
        if (url) {
            let parts = url.split('=');
            let id = '';
            if (parts && parts.length) {
                id = parts[1].split('&')[0];
            }
            let fullUrl = `https://www.youtube.com/embed/${id}`;
            let finalurl = this.domsant.bypassSecurityTrustResourceUrl(fullUrl);
            return finalurl;
        }

    }

    onImageChanged(image: string) {
        this.itemImage.image = image;
    }

    onSubmitVideo(f: NgForm) {
        if (f.valid) {
            this.loading = true;
            if (this.isEdit) {
                this.data.Update('api/gallry/videos/update', this.itemVideo)
                    .subscribe(
                        data => {
                            this.ngOnInit();
                            f.reset();
                            this.showSuccess();
                            this.resetVideo();
                            this.isEdit = false;
                            this.isForm = false;
                            this.showOneOF(false);
                            this.loading = false;
                        },
                        error => {
                            this.loading = false;
                            this.showError();
                        });
            } else {
                this.data.post('api/gallry/videos/add', this.itemVideo)
                    .subscribe(
                        data => {
                            this.ngOnInit();
                            f.reset();
                            this.showSuccess();
                            this.resetVideo();
                            this.loading = false;
                            this.isForm = false;
                            this.showOneOF(false);
                        },
                        error => {
                            this.loading = false;
                            this.showError();
                        });
            }
        }
    }


    onSubmitImage(f: NgForm) {
        if (f.valid) {
            this.loading = true;
            if (this.isEdit) {
                this.data.Update('/api/gallry/images/update', this.itemImage)
                    .subscribe(
                        data => {
                            this.ngOnInit();
                            f.reset();
                            this.showSuccess();
                            this.restImage();
                            this.isEdit = false;
                            this.isForm = false;
                            this.showOneOF(true);
                            this.loading = false;
                        },
                        error => {
                            this.loading = false;
                            this.showError();
                        });
            } else {
                this.data.post('/api/gallry/images/add', this.itemImage)
                    .subscribe(
                        data => {
                            this.ngOnInit();
                            f.reset();
                            this.showSuccess();
                            this.restImage();
                            this.loading = false;
                            this.isForm = false;
                            this.showOneOF(true);
                        },
                        error => {
                            this.loading = false;
                            this.showError();
                        });
            }
        }
    }


    markImageAsDelete(it) {
        this.itemImage = it;
    }

    markVideoAsDelete(it) {
        this.itemVideo = it;
    }




    restImage() {
        this.itemImage = {
            title: "",
            image: "",
            decription: ""
        }
    }

    resetVideo() {
        this.itemVideo = {
            title: "",
            videoUrl: "",
            decription: "",
            isYoutube: true
        }
    }

    removeImage() {
        this.data.Delete('/api/gallry/images/delete', this.itemImage)
            .subscribe(
                () => {
                    this.showDeletd();
                    this.isForm = false;
                    this.restImage();
                    this.ngOnInit();
                },
                error => {
                    this.loading = false;
                    this.showError();
                });
    }

    removeVideo() {
        debugger
        this.data.Delete('/api/gallry/videos/delete', this.itemVideo)
            .subscribe(
                () => {
                    this.showDeletd();
                    this.isForm = false;
                    this.resetVideo();
                    this.ngOnInit();
                },
                error => {
                    this.loading = false;
                    this.showError();
                });
    }


    editImage(obj) {
        this.isEdit = true;
        this.isForm = true;
        this.isImage = true;
        this.itemImage = obj;
    }

    editVideo(obj) {
        this.isEdit = true;
        this.isForm = true;
        this.isVideo = true;
        this.itemVideo = obj;
    }

    ngOnInit() {

        this.data.get('/api/gallry/videos/all').subscribe((data) => {
            console.log(data);
            this.videos = data;
        })

        this.data.get('/api/gallry/images/all').subscribe((data) => {
            console.log(data);
            this.images = data;
        })



    }


    toggleView(isForm, isVideo, isImage, showImage) {
        this.isForm = isForm;
        this.isVideo = isVideo;
        this.isImage = isImage;

        this.resetVideo();
        this.restImage();
    }

    showOneOF(show) {
        this.showOne = show;
    }

    showSuccess() {
        this.toastr.success('نجاح', 'تم حفظ البيانات');
    }

    showError() {
        this.toastr.error('اخفاق ', 'فشل حفظ البيانات');
    }
    showDeletd() {
        this.toastr.success('تم الحذف', 'نجاح');
    }
}
