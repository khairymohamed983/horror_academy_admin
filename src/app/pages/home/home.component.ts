import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ScriptLoaderService } from '../../_services/script-loader.service';
import { DataService } from '../../_services/data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit, AfterViewInit {

  constructor(private _script: ScriptLoaderService, private data: DataService) { }

  stats = {
    applications: 0,
    players: 0,
    championships: 0,
    newses: 0,
    partners: 0,
    messages: 0,
    users: 0,
    gallary: 0
  }

  ngOnInit() {
    this.data.get('/api/other/statistics').subscribe((stat) => {
      console.log(stat);
      this.stats = stat;
    })
  }

  ngAfterViewInit() {
    this._script.load('./assets/js/scripts/dashboard_1_demo.js');
  }

}
