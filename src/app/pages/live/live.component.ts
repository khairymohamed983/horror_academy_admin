﻿import { Component, OnInit } from '@angular/core';
import { DataService } from "../../_services/data.service";
import { NgForm } from '@angular/forms';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { log, debug } from 'util';

import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-live',
    templateUrl: './live.component.html',
    styleUrls: ['./live.component.css']
})
export class LiveComponent implements OnInit {
    livevideos = [];
    loading = false;
    loadingvideo = false;
    isForm = false;
    currentUrl: SafeResourceUrl;
    currentUrlPop: SafeResourceUrl;
    itemPop =
        {
            title: null,
            url: null,
            isCurrent: true

        }

    item = {
        title: null,
        url: null,
        isCurrent: true
    }
    isEdit = false;

      











showSuccess() {
    this.toastr.success('نجاح', 'تم حفظ البيانات');
}

showError() {
    this.toastr.error('اخفاق ', 'فشل حفظ البيانات');
}

constructor(private data: DataService, public domsant: DomSanitizer, private toastr: ToastrService) {
    }

    urlChanged($event) {
        let parts = this.item.url.split('=');
        let id = '';
        if (parts && parts.length) {
            id = parts[1].split('&')[0];
        }
        let fullUrl = `https://www.youtube.com/embed/${id}`;
        this.loadingvideo = true;
        this.currentUrl = this.domsant.bypassSecurityTrustResourceUrl(fullUrl);
    }

    adjustUrl(url) {
        debugger
        if (url) {
            debugger
            let parts = url.split('=');
            let id = '';
            if (parts && parts.length) {
                id = parts[1].split('&')[0];
            }
            let fullUrl = `https://www.youtube.com/embed/${id}`;
            debugger
            let finalurl = this.domsant.bypassSecurityTrustResourceUrl(fullUrl);
            return finalurl;
        }

    }

    onSubmit(f: NgForm) {
        if (f.valid) {
            this.loading = true;
            this.data.post('/api/other/live/add', this.item)
                .subscribe(
                data => {
                    this.ngOnInit();
                    f.reset();
                    this.showSuccess();
                    this.reset();
                    this.loading = false;
                    this.isForm = false;
                },
                error => {
                    this.loading = false;
                    this.showError();
                });
        }
    }


    markAsDelete(it) {
        this.itemPop = it;
        this.currentUrlPop = this.adjustUrl(it.url);
    }

    reset() {
        this.item = {
            title: null,
            url: null,
            isCurrent: true
        }

    }

 
    ngOnInit() {
        this.data.get('/api/other/live/history').subscribe((data) => {
            console.log(data);
            this.livevideos = data;
        })
    }
}
