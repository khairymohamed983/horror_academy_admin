import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
declare var $: any;
import { UserService } from "../../_services/user.service";
import { ActivatedRoute, Router } from '@angular/router';
import { FormsModule, NgForm } from '@angular/forms'; // <-- NgModel lives here

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
})

export class LoginComponent implements OnInit, AfterViewInit, OnDestroy {
    loading = false;
    returnUrl = '/';

    constructor(private userSvc: UserService,
        private route: ActivatedRoute,
        private router: Router) {
        if (userSvc.isLoggedIn()) {
            this.router.navigate([this.returnUrl]);
        }
    }

    onSubmit(f: NgForm) {
        this.loading = true;
        console.log(f);
        this.userSvc.login(f.controls.username.value
            , f.controls.password.value)
            .subscribe(
                data => {
                    location.href = this.returnUrl;
                },
                error => {
                    this.loading = false;
                });
    }

    ngOnInit() {
        $('body').addClass('empty-layout bg-silver-300');
    }

    ngAfterViewInit() {
        // $('#login-form').validate({
        //     errorClass: "help-block",
        //     rules: {
        //         username: {
        //             required: true,
        //         },
        //         password: {
        //             required: true
        //         }
        //     },
        //     highlight: function (e) {
        //         $(e).closest(".form-group").addClass("has-error")
        //     },
        //     unhighlight: function (e) {
        //         $(e).closest(".form-group").removeClass("has-error")
        //     },
        // });
    }


    ngOnDestroy() {
        $('body').removeClass('empty-layout bg-silver-300');
    }

}
