import { Component, OnInit, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { DataService } from "../../_services/data.service";
import { NgForm } from '@angular/forms';
import { debug } from 'util';

@Component({
    selector: 'app-messages',
    templateUrl: './messages.component.html',
    styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {

    messages = [];
 

    item = {
        name: null,
        issuesDate: null,
        message: null,
        email: null
    }

    constructor(private data: DataService) {
    }

    markAsDelete(it) {
         this.item = it;
    }

    removeItem() {
        debugger
        this.data.Delete('api/other/contactMessages/delete',this.item)
            .subscribe(
            () => {            
                this.ngOnInit();
                this.reset();
            },
            error => {
                console.log(error);
            });
    }

    reset() {
        this.item = {
            name: null,
            issuesDate: null,
            message: null,
            email: null
        }
    }


    ngOnInit() {
        this.data.get('/api/other/contactMessages/all').subscribe((data) => {
            console.log(data);
            this.messages = data;
        });
    }

    showDetails(it) {
        this.item = it;
    }
}


