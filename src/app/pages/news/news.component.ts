﻿import { Component, OnInit } from '@angular/core';
import { DataService } from "../../_services/data.service";
import { NgForm } from '@angular/forms';
import { debug } from 'util';
import { ScriptLoaderService } from '../../_services/script-loader.service';
import { IMyDpOptions } from 'mydatepicker';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-news',
    templateUrl: './news.component.html',
    styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

    news = [];
    loading = false;
    isForm = false;
    isEdit = false;



    postDateOptions = {};

     //public postDate: any = { date: { year: 2018, month: 10, day: 9 } };

    public myDatePickerOptions: IMyDpOptions = {

        // other options...
        dateFormat: 'dd.mm.yyyy',
    };

    item = {
        title: null,
        image: null,
        decription: null,
        postDate: null
    }

constructor(private data: DataService, private _script: ScriptLoaderService, private toastr: ToastrService) {
        this.data.setCurrent('news');
    }

    onImageChanged(image: string) {
        this.item.image = image;
    }

    onSubmit(f: NgForm) {
        debugger
        console.log(this.item.postDate.jsdate);
        this.item.postDate = this.item.postDate.formatted;
        if (f.valid) {
            this.loading = true;
            if (this.isEdit) {
                debugger
                this.data.update(this.item)
                    .subscribe(
                    data => {
                        this.ngOnInit();
                        f.reset();
                        this.showSuccess();
                        this.reset();
                        this.isEdit = false;
                        this.isForm = false;
                        this.loading = false;
                    },
                    error => {
                        this.loading = false;

                        this.showError();
                    });
            } else {
                this.data.add(this.item)
                    .subscribe(
                    data => {
                        this.ngOnInit();
                        f.reset();
                        this.showSuccess();
                        this.reset();
                        this.loading = false;
                        this.isForm = false;
                    },
                    error => {
                        this.loading = false;

                        this.showError();
                    });
            }
        }
    }

    markAsDelete(it) {
        this.item = it;
    }

    reset() {
        this.item = {
            title: null,
            image: null,
            decription: null,
            postDate: null
        }
    }

    removeItem() {
        this.data.delete(this.item)
            .subscribe(
            () => {
                this.showDeletd();
                this.isForm = false;
                this.reset();
                this.ngOnInit();
            },
            error => {
                this.showError();
                this.loading = false;
            });
    }

    editItem(obj) {
        this.isEdit = true;
        this.isForm = true;
        this.item = obj;
    }
    ngAfterViewInit() {
        this._script.load('./assets/js/scripts/form-plugins.js');
    }

    ngOnInit() {
        this.data.getAll().subscribe((data) => {
            console.log(data);
            this.news = data;
        })
        console.log(this.news);
    }

    toggleView(isForm) {
        this.isForm = isForm;
    }

    showSuccess() {
        this.toastr.success('نجاح', 'تم حفظ البيانات');
    }

    showError() {
        this.toastr.error('اخفاق ', 'فشل حفظ البيانات');
    }
    showDeletd() {
        this.toastr.success('تم الحذف', 'نجاح');
    }
}
