﻿import { Component, OnInit } from '@angular/core';
import { DataService } from "../../_services/data.service";
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-partners',
    templateUrl: './partners.component.html',
    styleUrls: ['./partners.component.css']
})
export class PartnersComponent implements OnInit {


    partners = [];
    loading = false;
    isForm = false;
    item = {
        title: null,
        image: null,
        decription: null,
        name: null
    }
    isEdit = false;

    constructor(private data: DataService, private toastr: ToastrService) {
        this.data.setCurrent('partners');
    }

    onImageChanged(image: string) {
        this.item.image = image;
    }


    onSubmit(f: NgForm) {
        console.log(this.item.image);
        if (f.valid) {
            this.loading = true;
            if (this.isEdit) {
                this.data.update(this.item)
                    .subscribe(
                    data => {
                        this.ngOnInit();
                        f.reset();
                        this.showSuccess();
                        this.reset();
                        this.isEdit = false;
                        this.isForm = false;
                        this.loading = false;
                    },
                    error => {
                        this.loading = false;
                        this.showError();

                    });
            } else {
                this.data.add(this.item)
                    .subscribe(
                    data => {
                        this.ngOnInit();
                        f.reset();
                        this.showSuccess();
                        this.reset();
                        this.loading = false;
                        this.isForm = false;
                    },
                    error => {
                        this.loading = false;
                        this.showError();
                    });
            }
        }
    }

    markAsDelete(it) {
        this.item = it;
    }

    reset() {
        this.item = {
            title: null,
            image: null,
            decription: null,
            name: null
        }
    }

    removeItem() {
        this.data.delete(this.item)
            .subscribe(
            () => {
                this.showDeletd();
                this.isForm = false;
                this.reset();
                this.ngOnInit();
            },
            error => {
                this.loading = false;
                this.showError();
            });
    }

    editItem(obj) {
        this.isEdit = true;
        this.isForm = true;
        this.item = obj;
    }

    ngOnInit() {
        this.data.getAll().subscribe((data) => {
            console.log(data);
            this.partners = data;
        })
    }
    toggleView(isForm) {
        this.isForm = isForm;
    }

    showSuccess() {
        this.toastr.success('نجاح', 'تم حفظ البيانات');
    }

    showError() {
        this.toastr.error('اخفاق ', 'فشل حفظ البيانات');
    }
    showDeletd() {
        this.toastr.success('تم الحذف', 'نجاح');
    }

}
