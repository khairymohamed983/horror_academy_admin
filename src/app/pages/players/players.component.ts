﻿import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { DataService } from "../../_services/data.service";
import { NgForm } from '@angular/forms';
import { ScriptLoaderService } from '../../_services/script-loader.service';
import { IMyDpOptions } from 'mydatepicker';
import { ToastrService } from 'ngx-toastr';

 


@Component({
    selector: 'app-players',
    templateUrl: './players.component.html',
    styleUrls: ['./players.component.css']
})
export class PlayersComponent implements OnInit {

    players = [];
    loading = false;
    isForm = false;
    public myDatePickerOptions: IMyDpOptions = {
        // other options...
        dateFormat: 'dd.mm.yyyy',
    };


    // Initialized to specific date (09.10.2018).
    //public birthDate: any = { date: { year: 2018, month: 10, day: 9 } };
    //public subscriptionDate: any = { date: { year: 2018, month: 10, day: 9 } };

    item = {
        name: null,
        image: null,
        birthDate: null,
        position: null,
        subscriptionDate: null,
        decription: null
    }

    isEdit = false;
    birthDateOptions = {};
    suscribeDateOptions = {};
    constructor(private data: DataService, private _script: ScriptLoaderService, private toastr: ToastrService) {
        this.data.setCurrent('players');
    }


    onDateChanged($event) {

    }

    onImageChanged(image: string) {
        this.item.image = image;
    }

    onSubmit(f: NgForm) {
        debugger

        //not finished 
        this.item.birthDate = this.item.birthDate.formatted;
        this.item.subscriptionDate = this.item.subscriptionDate.formatted; 
        if (f.valid) {
            this.loading = true;
            if (this.isEdit) {
                this.data.update(this.item)
                    .subscribe(
                    data => {
                        this.ngOnInit();
                        f.reset();
                        this.reset();
                        this.showSuccess();
                        this.isEdit = false;
                        this.isForm = false;
                        this.loading = false;
                    },
                    error => {
                        this.loading = false;
                        this.showError();
                        console.log(error);
                    });
            } else {
                this.data.add(this.item)
                    .subscribe(
                    data => {
                        this.ngOnInit();
                        f.reset();
                        this.reset();
                        this.showSuccess();
                        this.loading = false;
                        this.isForm = false;
                    },
                    error => {
                        this.loading = false;
                        this.showError();
                        console.log(error);
                    });
            }
        }
    }

    markAsDelete(it) {
        this.item = it;
    }

    reset() {
        this.item = {
            name: null,
            image: null,
            birthDate: null,
            position: null,
            subscriptionDate: null,
            decription: null
        }
    }

    removeItem() {
        this.data.delete(this.item)
            .subscribe(
            () => {
                this.showDeletd();
                this.isForm = false;
                this.reset();
                this.ngOnInit();
            },
            error => {
                this.loading = false;
                this.showError();
            });
    }

    editItem(obj) {
        this.isEdit = true;
        this.isForm = true;
        this.item = obj;
    }

    ngAfterViewInit() {
        this._script.load('./assets/js/scripts/form-plugins.js');
    }

    ngOnInit() {
        this.data.getAll().subscribe((data) => {
            console.log(data);
            this.players = data;
        })
    }
    toggleView(isForm) {
        this.isForm = isForm;
    }


    showSuccess() {
        this.toastr.success('نجاح', 'تم حفظ البيانات');
    }

    showError() {
        this.toastr.error('اخفاق ', 'فشل حفظ البيانات');
    }
    showDeletd() {
        this.toastr.success('تم الحذف', 'نجاح');
    }


}
