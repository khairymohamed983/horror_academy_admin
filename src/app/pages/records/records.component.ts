﻿import { log } from 'util';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { DataService } from "../../_services/data.service";
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';


@Component({
    selector: 'app-records',
    templateUrl: './records.component.html',
    styleUrls: ['./records.component.css']
})
export class RecordsComponent implements OnInit {


    loading = false;
    isEditMember = false;


    @ViewChild('PopUp') PopUp: ElementRef;

    @ViewChild('model', { read: ElementRef }) model: ElementRef;

    index = undefined;


    item: any = {
        noGoals: 0,
        noPlayers: 0,
        noEvants: 0,
        noPrize: 0,
        players: []
    }

    item2 = {
        image: null,
        name: null,
        category: null,
        reason: null
    }


    constructor(private data: DataService, private toastr: ToastrService) {
        this.data.setCurrent('records');
    }



    onImageChangedMembers(image: string) {
        this.item2.image = image;
    }

    onSubmit(f: NgForm) {
        debugger;
        if (f.valid) {
            this.loading = true;
            
            this.data.add(this.item)
                .subscribe(
                    data => {
                        this.ngOnInit();
                        this.loading = false;
                        this.showSuccess();
                    },
                    error => {
                        this.loading = false;
                        this.showError();
                    });



        }
    }


    ngOnInit() {
        this.data.getAll().subscribe((data) => {
            debugger;
            if (data.length > 0)
                this.item = data[0];
        })
    }





    onSubmitMember() {
        if (this.isEditMember && this.index != undefined) {
            this.popUpDismiss();
            this.item.players[this.index] = this.item2;
            this.resetMember();
        }
        else {
            this.item.players.push(this.item2);
            this.popUpDismiss();
            this.resetMember();

        }
    }


    resetMember() {
        this.item2 = {
            image: null,
            name: null,
            category: null,
            reason: null
        }
    }

    deleteMember(index) {
        if (index >= 0) {
            this.item.players.splice(index, 1);
        }
    }
    setState() {
        this.isEditMember = false;
        this.resetMember();
    }


    updateMember(obj, index2) {
        this.item2 = obj;
        this.index = index2;
        this.isEditMember = true;
    }


    popUpDismiss() {
        this.PopUp.nativeElement.click();
    }

    showSuccess() {
        this.toastr.success('نجاح', 'تم حفظ البيانات');
    }

    showError() {
        this.toastr.error('اخفاق ', 'فشل حفظ البيانات');
    }

}
