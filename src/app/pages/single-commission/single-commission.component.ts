import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-single-commission',
  templateUrl: './single-commission.component.html',
  styleUrls: ['./single-commission.component.css']
})
export class SingleCommissionComponent implements OnInit {

  constructor() { }
  commission = {};

  ngOnInit() {
    this.commission = JSON.parse(localStorage.getItem('currentCommission'));
  }
}
